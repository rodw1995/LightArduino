#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include "config.h"

MDNSResponder mdns;

ESP8266WebServer server(80);

void setup(void){  
  delay(1000);
  Serial.begin(115200);

  // Setup LED
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, LOW);

  // Setup wifi
  WiFi.begin(ssid, password);
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  
  if (mdns.begin("esp8266", WiFi.localIP())) {
    Serial.println("MDNS responder started");
  }
  
  server.on("/toggleLight", []() {
    // Check token is valid
    if (server.hasArg("token")) {
      auto token = server.arg("token");
      auto response = get_request(CHECK_TOKEN_HOST, CHECK_TOKEN_PATH + "/" + token);

      // Read response
      if (response.indexOf("true") > 0) {
        // Toggle led
        digitalWrite(LED_PIN, !digitalRead(LED_PIN));
    
        // Send response
        String state;
        if (digitalRead(LED_PIN) == HIGH) {
          state = "ON";
        } else {
          state = "OFF";
        }
        
        server.send(200, "application/json", "{\"success\": true, \"state\": \"" + state + "\"}");
      } else {
        server.send(200, "application/json", "{\"success\": false, \"message\": \"Invalid token\"}");
      }
    } else {
      server.send(200, "application/json", "{\"success\": false, \"message\": \"Token needed to authenticate request\"}");
    }
    
    delay(1000);
  });

  server.begin();
  Serial.println("HTTP server started");
}
 
void loop(void){
  server.handleClient();
}

bool connect_with_wifi(WiFiClient& client, const char *host, uint16_t port) {
  Serial.print("Connecting to ");
  Serial.println(host);
  Serial.println(port);
  
  if (!client.connect(host, port)) {
    Serial.println("connection failed");
    return false;
  }

  return true;
}

String get_request(const char *host, String url) {
  return get_request(host, url, 80);
}

String get_request(const char *host, String url, uint16_t port) {
  // Connect to the client
  WiFiClient client;

  String response = "";
  if (connect_with_wifi(client, host, port)) {
    // Send request
    Serial.println("Send request:");
    
    auto request = "GET " + url + " HTTP/1.1\r\n" +
                 "Host: " + host + "\r\n" +
                 "Connection: close\r\n\r\n";
    
    Serial.println(request);
    client.print(request);
    
    Serial.println("request sent");

    // http://www.esp8266.com/viewtopic.php?f=32&t=6463
    int timeout = millis() + 5000;
    while(client.available() == 0) {
      if (timeout - millis() < 0) {
        Serial.println("Client Timeout!");
        client.stop();
        return response;
      }
    }
    
    while(client.available()) {
      String line = client.readStringUntil('\r');
      Serial.print(line);
      response += line;
    }
    
    // Disconnect
    Serial.println();
    Serial.println("disconnecting from server.");
    client.stop();
  }
  
  return response;
}
